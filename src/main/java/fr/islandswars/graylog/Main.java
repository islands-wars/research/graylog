package fr.islandswars.graylog;

import java.util.Random;

/**
 * File <b>Main</b> located on fr.islandswars.graylog
 * Main is a part of Islands Wars - Graylog.
 * <p>
 * Copyright (c) 2017 - 2018 Islands Wars.
 *
 * @author Valentin Burgaud (Xharos), {@literal <xharos@islandswars.fr>}
 * Created the 16/01/2018 at 12:45
 * @since 1.0.0
 */
public class Main {

	public static void main(String[] args) {
		long   start  = System.currentTimeMillis() + 1000;
		Random random = new Random();
		while (true) {
			if (System.currentTimeMillis() >= start) {
				if (random.nextFloat() >= 0.5) {
					System.err.println("Info message id ->" + random.nextInt(1000));
				} else {
					System.err.println("Error message id ->" + random.nextInt(1000));
				}
				start = System.currentTimeMillis() + 1000;
			}
		}
	}
}
