# <img src="https://islands-wars.gitlab.io/assets/brands/icons/icon.svg" width="64"> Graylog


> Islands Wars is a Minecraft sky block server.

> Graylog manager test through docker and simple java app

---

## Setup

Simply clone this repo and follow the steps

### Graylog

Launch in your terminal :
```
docker-compose up -d

```
It will pull and run a basic graylog environments according to 2.4.0 graylog official [documentations](http://docs.graylog.org/en/2.4/pages/installation/docker.html#settings).
You can connect through graylog using ``docker-machine-ip:9000`` and with user = admin, password = admin credentials.
Then, just create a new inputs in System options, with GELF UDP and press 'Launch new input'.

![result](https://www.ctl.io/developers/assets/images/blog/graylog_GELF_input_selection.png)


## Launch test

Simply execute :
```
./gradlew jar
docker build . -t test-graylog
```
And start this container with log options and driver parameters :
```
docker run --log-driver gelf --log-opt gelf-address=udp://docker-machine-ip:12201 test-graylog
```

---

# CI/CD

* [master](https://gitlab.com/islands-wars/research/graylog/tree/master) - ![status](https://gitlab.com/islands-wars/research/graylog/badges/master/build.svg)

# License

> Do What The Fuck You Want to Public License, see [LICENSE](https://gitlab.com/islands-wars/research/graylog/blob/master/LICENSE) file.
