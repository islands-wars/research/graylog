FROM openjdk:8-alpine

MAINTAINER Burgaud Valentin "xharos@islandswars.fr"

ADD build/libs/graylog.jar graylog.jar

CMD ["java", "-jar", "graylog.jar"]